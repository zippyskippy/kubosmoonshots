import React from 'react';
import { useState } from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import EmojiObjectsIcon from '@material-ui/icons/EmojiObjects';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import ScheduleIcon from '@material-ui/icons/Schedule';

import { getMockData } from '../data/feed.js'
import Database from '../data/feed.js'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginBottom: "1em",
    position: "sticky",
    top: 0,
  },
  title: {
    marginLeft: "1em",
    align: "left",
    flexGrow: 1,
  },
  btn: { color: "white", backgroundColor: "rgb(78, 78, 190)" },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'left',
    marginLeft: '10px'
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

export default function SearchBar(props) {
  const classes = useStyles();

  const [msg, setMsg] = useState();
  const [oldTerms, setOld] = useState(props.terms || '');
  const [terms, setTerms] = useState(oldTerms);
  const [stamp, setStamp] = useState(props.start || '');

  const onSearch = query => {

    //first, parse the start timestamp
    let stamps = query.match(/t\d+/);
    if (stamps) {
      setStamp(stamps[0].replace('t', ''));
      setTerms(query.replace(stamps[0], ''));
    } else {
      setTerms(query);
    }

    //next, perform search
    Database.search({ query: query, filters: 'telemetry_timestamp >= ' + (Number.parseInt(stamp) || 0) }, (err, data) => {
      if (err) throw err;
      props.setData(data.hits);
      setMsg("Search found " + data.nbHits + " records");
    });
  }


  //if props.terms change, then update the terms, otherwise the user edits
  if (props.terms !== oldTerms) {
    setOld(props.terms);
    onSearch(props.terms);
  }


  return (
    <div className={classes.root}>
      <AppBar position="sticky">
        <Toolbar>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <ScheduleIcon />
            </div>
            <InputBase
              placeholder="Start time stamp..."
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'timestamp' }}
              value={stamp}
              onChange={e => {
                setStamp(e.currentTarget.value);
                onSearch(terms);
              }}
            />
          </div>


          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              value={terms}
              onChange={e => {
                onSearch(e.currentTarget.value);
              }}
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>

          or use &nbsp;
          <Button className={classes.btn} variant="outlined"
            onClick={e => {
              setTerms("");
              props.setData(getMockData());
              setMsg("The following is mocked data");
            }}
          >
            <EmojiObjectsIcon />Mock Data
          </Button>



        </Toolbar>
      </AppBar>
      <div><strong>Notice:</strong> to search for satellite 3  batch 5 ,  enter s3 b5 in the search terms.  </div>
      <div> You can also search for any terms in errors, status, etc.</div>
      {msg}
    </div>
  );
}