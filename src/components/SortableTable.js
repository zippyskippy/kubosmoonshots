import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import AlarmIcon from '@material-ui/icons/Alarm';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import LocalDrinkIcon from '@material-ui/icons/LocalDrink';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';


import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        zIndex: '-999'
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    sticky: {
        position: "sticky",
        top: 0,
    },
    tableWrapper: {
        overflowX: 'auto',
    },

}));


function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}


function ErrorList(list) {
    const unique = (list) => { return [...new Set(list)] };

    return (
        <List>
            {unique(list).sort().map(itm => {
                return <ListItem key={itm}>{itm}</ListItem>
            })}
        </List>
    )
}

function StatusIcon(props) {
    const status = props.status;
    if (!status) return null;

    if (status === "aging") return <AccessTimeIcon />;
    if (status === "error") return <ErrorOutlineIcon />;
    if (status === "ready") return <CheckCircleOutlineIcon />;
    if (status === "expired") return <AlarmIcon />;
    if (status === "empty") return <LocalDrinkIcon />;
    return <HelpOutlineIcon />;
}

function StatusClass(status) {
    const KnownStatusList = ["aging", "error", "ready", "expired", "empty"];
    if (KnownStatusList.indexOf(status) > -1) return status;
    else return "unknown";
}


const headCells = [
    { id: 'telemetry_timestamp', label: 'time', prefix: 't' },
    { id: 'satellite_id', label: 'sat id', prefix: 's' },
    { id: 'batch_id', label: 'batch id', prefix: 'b' },
    { id: 'last_flavor_sensor_result', label: 'flavor', },
    {
        id: 'status', label: 'status',
        format: value => <div className={StatusClass(value)}>
            <StatusIcon status={value} />
            <div>
                {value}
            </div>
        </div>
    }, {
        id: 'errors', label: 'errors',
        format: values => ErrorList(values)
    },
];

function EnhancedTableHead(props) {
    const { classes, order, orderBy, onRequestSort } = props;
    const createSortHandler = property => event => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>

                {headCells.map(headCell => (
                    <TableCell
                        className={classes.sticky}
                        key={headCell.id}
                        align={headCell.align || 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={order}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label.toUpperCase()}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};





export default function SortableTable(props) {
    const rows = props.rows || [];

    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('time');
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const onSearch = props.onSearch;

    const handleRequestSort = (event, property) => {
        const isDesc = orderBy === property && order === 'desc';
        setOrder(isDesc ? 'asc' : 'desc');
        setOrderBy(property);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>

                <div className={classes.tableWrapper}>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size="small"
                        aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                            rowCount={rows.length}
                        />
                        <TableBody>
                            {stableSort(rows, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {

                                    return (
                                        <TableRow
                                            hover
                                            tabIndex={-1}
                                            key={index}
                                        >

                                            {headCells.map(itm => {
                                                return <TableCell key={itm.id} align={itm.align || 'left'} >
                                                    {
                                                        <div className="pointer" onClick={e => {
                                                            onSearch((itm.prefix || '') + row[itm.id]);
                                                        }}>
                                                            {itm.format ? itm.format(row[itm.id]) : row[itm.id]}
                                                        </div>
                                                    }
                                                </TableCell>
                                            })}

                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 33 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 50, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    );
}
