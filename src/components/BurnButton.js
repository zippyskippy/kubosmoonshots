import React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Container from '@material-ui/core/Container';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

import TextField from '@material-ui/core/TextField';


function BurnDialog(props) {
    const { onClose, open } = props;

    const handleClose = () => {
        onClose();
    };


    const [satellite, setSat] = useState();

    return (
        <Dialog onClose={handleClose} aria-labelledby="burn baby burn" open={open}>
            <DialogTitle>Are you sure you want to destroy a satellite? </DialogTitle>
            <Container>
                <Container>To ensure you're sure, you must enter a correct satellite ID and click a button.  We will send a confirmation email to your supervisor.

                    <TextField label="Satellite ID" placeholder="Enter the satellite ID" value={satellite} type="number"
                        onChange={e => {
                            setSat(e.currentTarget.value);
                        }}
                    />
                </Container>
                <Container onClick={e => props.onClose()}>
                    <Button variant="outlined" color="secondary" disabled={!satellite}>Burn</Button>
                    <Button variant="outlined" color="secondary" disabled={!satellite}>Detonate</Button>
                    <Button variant="outlined" color="primary" >Cancel</Button>
                </Container>
            </Container>
        </Dialog>
    );
}


BurnDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
};

export default function BurnButton() {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = value => {
        setOpen(false);
    };

    return (
        <div>
            <Button variant="outlined" color="secondary" onClick={handleClickOpen}>
                <DeleteForeverIcon /> Delete Satellite
            </Button>
            <BurnDialog open={open} onClose={handleClose} />
        </div>
    );
}
