import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        margin: '1em'
    },
}));

export default function BigGrid(props) {
    const children = props.children || ["one", "two"];
    const num = children.length;

    const classes = useStyles();


    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                {(num === 1) && <Grid item xs={12}>
                    <Paper className={classes.paper}>{children}</Paper>
                </Grid>}

                {(num === 2) && <>
                    <Grid item xs={6} >
                        <Paper className={classes.paper}>{children[0]}</Paper>
                    </Grid>
                    <Grid item xs={6} >
                        <Paper className={classes.paper}>{children[1]}</Paper>
                    </Grid>
                </>}

                {(num === 3) && <>
                    <Grid item xs={4} >
                        <Paper className={classes.paper}>{children[0]}</Paper>
                    </Grid>
                    <Grid item xs={4} >
                        <Paper className={classes.paper}>{children[1]}</Paper>
                    </Grid>
                    <Grid item xs={4} >
                        <Paper className={classes.paper}>{children[2]}</Paper>
                    </Grid>
                </>}




                {/* <Grid item xs={6} sm={3}>
                    <Paper className={classes.paper}>xs=6 sm=3</Paper>
                </Grid>
                <Grid item xs={6} sm={3}>
                    <Paper className={classes.paper}>xs=6 sm=3</Paper>
                </Grid>
                <Grid item xs={6} sm={3}>
                    <Paper className={classes.paper}>xs=6 sm=3</Paper>
                </Grid>
                <Grid item xs={6} sm={3}>
                    <Paper className={classes.paper}>xs=6 sm=3</Paper>
                </Grid> */}
            </Grid>
        </div>
    );
}
