import React from 'react';
import {useState} from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';

import BurnButton from './BurnButton'
import Database from '../data/feed';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginBottom: "1em",
    position: "sticky",
    top: 0,
  },
  title: {
    marginLeft: "1em",
    align: "left",
    flexGrow: 1,
  },
  btn: { marginRight: '1em', color: "white", backgroundColor: "rgb(78, 78, 190)" },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'left',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));


export default function Banner(props) {
  const classes = useStyles();

  const [msg,setMsg] = useState();
  const setFileName = props.setFileName;


  const onUpload = ({ target }) => {
    const fileReader = new FileReader();
    const filename = target.files[0].name;
    setFileName(filename);
    fileReader.readAsText(target.files[0]);
    fileReader.onload = (e) => {
      const text = JSON.parse(e.target.result);
      Database.push(text, (err, data) =>{
        if (err) setMsg(err);
        else  setMsg(''+ filename+ ': ' + data.objectIDs.length + " records saved");

        setTimeout(function(){ setMsg(null) },5000); //clear any old error messages
      });
    }
  };


  return (
    <div className={classes.root}>
      <AppBar position="sticky">
        <Toolbar>
          <Avatar edge="start" alt="logo" src="/icons/favicon-96x96.png" className={classes.bigAvatar} />
          <Typography variant="h6" className={classes.title}>
            MoonShots by Chad Steele
          </Typography>

          <input
            style={{ display: 'none' }}
            id="fileinput"
            type="file"
            onChange={onUpload}
          />

          <label htmlFor="fileinput">
            <Button className={classes.btn} variant="outlined" component="span">
              <CloudUploadIcon /> &nbsp; Upload
              </Button>
          </label>



          <BurnButton variant="outlined" />

        </Toolbar>
        
      </AppBar>
      <div>{msg}</div>

    </div>
  )
};