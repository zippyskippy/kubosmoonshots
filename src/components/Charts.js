import React from 'react';
import BigGrid from './BigGrid.js';
import PieChart from 'react-minimal-pie-chart';


const defaultLabelStyle = {
    fontSize: '0.4em',
    fill: '#fff',
    textShadow: "1px 1px #000"
};

const colors = {
    "aging": "goldenrod",
    "error": "red",
    "ready": "green",
    "expired": "black",
    "empty": "gray",
    "unknown": "blue",
}

const unique = (list) => { return [...new Set(list)] };

function getUnique(fieldname, data) {
    const list = data.map(itm => {
        return itm[fieldname];
    });
    return unique(list);
}

function count(label, fieldname, data) {
    return data.reduce((acc, cur) => {
        return (cur[fieldname] === label) ? ++acc : acc;
    }, 0);
}


function getStats(label, data) {
    const labels = getUnique(label, data).sort();
    return labels.map((itm, i) => {
        const c = count(itm, label, data);
        return { title: itm, value: Math.floor(100 * c / data.length), color: colors[itm] }
    });
}



export default function Charts(props) {
    if (!props.data) return null;

    const stats = getStats("status", props.data);

    return <BigGrid>

        <div>
            <h3>Status</h3>
            {stats.map((itm, i) => {
                return <div key={i} style={{ color: colors[itm.title] }}>{itm.value + '% ' + itm.title}</div>
            })}
        </div>


        <PieChart animate={true}
            label={rec => {
                const title = rec.data[rec.dataIndex].title;
                const value = rec.data[rec.dataIndex].value;
                if (value > 4) return value + '% ' + title;
                return "";
            }}
            labelStyle={{
                ...defaultLabelStyle,
            }}
            data={stats}
        />

        <div>
            <h3>Flavors</h3>

            {getStats("last_flavor_sensor_result", props.data).map((itm, i) => {
                return <div key={i}>{itm.value + '% ' + itm.title}</div>
            })}

        </div>



    </BigGrid>


}