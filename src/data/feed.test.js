
import { getMockData } from './feed.js';
import {sample} from './sample.js'
import Database from './feed.js'

describe('mock data is legitimate', () => {
    const data = getMockData(50);

    it('mock data has 50 records', () => {
        expect(data.length).toBe(50);
    });

    /**
     * "telemetry_timestamp": Math.floor(Math.random() * 100000) + 1534198007,
        "satellite_id": Math.floor(Math.random() * 1000) + 1,
        "batch_id": Math.floor(Math.random() * 100) + 1,
        "last_flavor_sensor_result": options.flavors[Math.floor(Math.random() * options.flavors.length)],
        "errors": errors,
        "status": (errors.length) ? "error" : options.status[Math.floor(Math.random() * options.status.length)],
     * 
     */

    it('mock data has telemetry field', () => {
        expect(data[3].telemetry_timestamp).toBeGreaterThan(1000);
    });

    it('mock data has satellite_id', () => {
        expect(data[13].satellite_id).toBeGreaterThan(0);
    });

    it('mock data has batch_id', () => {
        expect(data[23].batch_id).toBeGreaterThan(0);
    });

    it('mock data has last_flavor_sensor_result', () => {
        expect(data[33].last_flavor_sensor_result).toBeDefined();
    });

    it('mock data has status', () => {
        expect(data[43].status).toBeDefined();
    });

})


describe('test a sample upload and then delete it', () => {

    const sampleIDs = ["s1b1-1534198007", "s1b2-1534198007", "s1b3-1534198007"];

    it('sample should flatten, upload, confirm and delete', done => {
        Database.push(sample, (error, results) => {
            if (error) throw error;

            expect(results.objectIDs.length).toBe(3);

            results.objectIDs.forEach(id => {
                expect(sampleIDs.indexOf(id)).toBeGreaterThan(-1);
            });

            Database.index.deleteObjects(sampleIDs, (error, results) => {
                if (error) throw error;
                done(); 
            });
        });
    });


})

describe('test 50 mock uploads and then delete it', () => {

    it('test mock upload', done => {
        const data = getMockData(50);
        Database.push(data, (error, results) => {
            if (error) throw error;
            expect(results.objectIDs.length).toBe(50);

            Database.index.deleteBy({ filters: 'type:mock' }, (error, results) => {
                if (error) throw error;
                done();
            });

        });
    });

})






