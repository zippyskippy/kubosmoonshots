import algoliasearch from 'algoliasearch';


const options = {
    flavors: ["woody", "spicy with a hint of radiation", "honey", "maple", "chocolate", "apple pie", "tuity fruity"],
    status: ["aging", "ready", "empty", "expired", "unknown"],
    errors: ["RUD - barrel has exploded", "RUD - barrel is leaking", "RUD - alien abduction", "RUD - barrel is missing", "RUD - barrel is missing"],
};


function randomErrors() {
    const num = Math.floor(Math.random() * options.errors.length);
    const list = [];
    const unique = (list) => { return [...new Set(list)] };

    for (let i = 0; i < num; i++) {
        list.push(options.errors[Math.floor(Math.random() * num)]);
    }
    return unique(list);
}

function randomBarrel() {
    const errors = randomErrors();

    //assume the data is flattened in a database, NOT the JSON sample format above used to upload the data
    return {
        "telemetry_timestamp": Math.floor(Math.random() * 100000) + 1534198007,
        "satellite_id": Math.floor(Math.random() * 1000) + 1,
        "batch_id": Math.floor(Math.random() * 100) + 1,
        "last_flavor_sensor_result": options.flavors[Math.floor(Math.random() * options.flavors.length)],
        "errors": errors,
        "status": (errors.length) ? "error" : options.status[Math.floor(Math.random() * options.status.length)],
        "type": "mock"
    };
}

export function getMockData(n = 100) {
    const list = [];
    for (let i = 0; i < n; i++) {
        list.push(randomBarrel());
    }
    return list;
}



// algolia stuff

export class Database {
    constructor(props) {
        props = props || {};
        props.key = props.key || '27a8578e0cf34208948257ea9c006f6a'; //admin

        props.indexName = props.indexName || 'moonshots';
        props.algolia = props.algolia || '3X9EIG6G4R';
        this.props = props;

        this.client = algoliasearch(props.algolia, props.key);
        this.index = this.client.initIndex(props.indexName);
    }

    push(data, callback) {

        if (!data) return false;

        //if I had time, I'd validate the data here, but for now we'll assume it's good enough

        if (data.barrels) {
            //flatten
            const { telemetry_timestamp, satellite_id, barrels, type } = data;

            const flat = barrels.map(barrel => {
                return Object.assign({}, barrel, {
                    UID: 's' + satellite_id + ' b' + barrel.batch_id + ' t' + telemetry_timestamp,
                    telemetry_timestamp: telemetry_timestamp,
                    satellite_id: satellite_id,
                    type: type
                });
            });

            data = flat;
        }

        this.index.addObjects(data, callback);
    }

    search(props, callback) {
        if (!props) return;
        console.log('db.search ' + JSON.stringify(props));
        this.index.search(props, callback);
    }

}

export default new Database();
