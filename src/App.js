import React from 'react';
import { useState, useEffect } from 'react';

import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';

import './App.css';
import Banner from './components/Banner.js';
import SortableTable from './components/SortableTable.js';
import Charts from './components/Charts.js';
import SearchBar from './components/SearchBar'

import ErrorIcon from '@material-ui/icons/Error';

function App() {
  const [data, setData] = useState([]);
  const [msg, setMsg] = useState();
  const [terms, setTerms] = useState("");
  const [filename, setFileName] = useState("your file");

  const onSearch = text => {
    setTerms(text);
  }

  //initial search
  useEffect(() => {
    setTerms("*");
  }, []);

  window.onerror = e => {
    // parsing error from uploading a non-json file
    if (e === "Script error.") {
      setMsg("Sorry!  We could not parse " + filename);
    } else {
      setMsg(e);
    }
    setTimeout(function () { setMsg(null) }, 5000);
  }

  return (
    <div className="App">
      <CssBaseline />
      <Banner setFileName={setFileName} />

      <Container>
        {msg && <Paper >
          <h1 style={{ color: "red" }}> <ErrorIcon />{msg}</h1>
        </Paper>}
        <SearchBar terms={terms} setData={setData} />
        {(data && data.length) ?
          <div>
            <h1>Summary</h1>
            <Charts data={data} />
            <h1>Details</h1>
            <div>Click on column titles to sort that column.  Click on items to search for that term.</div>
            <SortableTable rows={data} onSearch={onSearch} />
          </div>
          :
          <Paper >
            <h1 style={{ color: "red" }}> <ErrorIcon /> No data found that satisfies your search</h1>
          </Paper>
        }
      </Container>
    </div>
  );
}

export default App;
